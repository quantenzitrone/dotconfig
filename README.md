# dotconfig
outdated .config for my personal machine

Awesome config based on [WillPower3309/awesome-dotfiles](https://github.com/WillPower3309/awesome-dotfiles)
but the UI has changed quite a lot

Also some old i3 config.

My new config: [quantenzitrone/nix-config](https://codeberg.org/quantenzitrone/nix-config)
