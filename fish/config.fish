clear; neofetch
if status is-interactive
    #commands can go here
end
if test -n "$DESKTOP_SESSION"
    set -x (gnome-keyring-daemon --start | string split "=")
end
