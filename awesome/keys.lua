--	  ██╗  ██╗███████╗██╗   ██╗███████╗
--	  ██║ ██╔╝██╔════╝╚██╗ ██╔╝██╔════╝
--	  █████╔╝ █████╗   ╚████╔╝ ███████╗
--	  ██╔═██╗ ██╔══╝	╚██╔╝  ╚════██║
--	  ██║  ██╗███████╗   ██║   ███████║
--	  ╚═╝  ╚═╝╚══════╝   ╚═╝   ╚══════╝


-- ===================================================================
-- Initialization
-- ===================================================================


local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi

-- Define mod keys
local modkey = "Mod4"
local altkey = "Mod1"

-- define module table
local keys = {}


-- ===================================================================
-- Movement Functions (Called by some keybinds)
-- ===================================================================


-- Move given client to given direction
local function move_client(c, direction)
	-- If client is floating, move to edge
	if c.floating or (awful.layout.get(mouse.screen) == awful.layout.suit.floating) then
		local workarea = awful.screen.focused().workarea
		if direction == "up" then
			c:geometry({nil, y = workarea.y + beautiful.useless_gap * 2, nil, nil})
		elseif direction == "down" then
			c:geometry({nil, y = workarea.height + workarea.y - c:geometry().height - beautiful.useless_gap * 2 - beautiful.border_width * 2, nil, nil})
		elseif direction == "left" then
			c:geometry({x = workarea.x + beautiful.useless_gap * 2, nil, nil, nil})
		elseif direction == "right" then
			c:geometry({x = workarea.width + workarea.x - c:geometry().width - beautiful.useless_gap * 2 - beautiful.border_width * 2, nil, nil, nil})
		end
	-- Otherwise swap the client in the tiled layout
	elseif awful.layout.get(mouse.screen) == awful.layout.suit.max then
		if direction == "up" or direction == "left" then
			awful.client.swap.byidx(-1, c)
		elseif direction == "down" or direction == "right" then
			awful.client.swap.byidx(1, c)
		end
	else
		awful.client.swap.bydirection(direction, c, nil)
	end
end


-- Resize client in given direction
local floating_resize_amount = dpi(20)
local tiling_resize_factor = 0.05

local function resize_client(c, direction)
	if awful.layout.get(mouse.screen) == awful.layout.suit.floating or (c and c.floating) then
		if direction == "up" then
			c:relative_move(0, 0, 0, -floating_resize_amount)
		elseif direction == "down" then
			c:relative_move(0, 0, 0, floating_resize_amount)
		elseif direction == "left" then
			c:relative_move(0, 0, -floating_resize_amount, 0)
		elseif direction == "right" then
			c:relative_move(0, 0, floating_resize_amount, 0)
		end
	else
		if direction == "up" then
			awful.client.incwfact(-tiling_resize_factor)
		elseif direction == "down" then
			awful.client.incwfact(tiling_resize_factor)
		elseif direction == "left" then
			awful.tag.incmwfact(-tiling_resize_factor)
		elseif direction == "right" then
			awful.tag.incmwfact(tiling_resize_factor)
		end
	end
end


-- raise focused client
local function raise_client()
	if client.focus then
		client.focus:raise()
	end
end


-- ===================================================================
-- Mouse bindings
-- ===================================================================


-- Mouse buttons on the desktop
keys.desktopbuttons = gears.table.join(
	-- left click on desktop to hide notification
	awful.button({}, 1,
		function ()
			naughty.destroy_all_notifications()
		end
	)
)

-- Mouse buttons on the client
keys.clientbuttons = gears.table.join(
	-- Raise client
	awful.button({}, 1,
		function(c)
			client.focus = c
			c:raise()
		end
	),

	-- Move and Resize Client
	awful.button({modkey}, 1, awful.mouse.client.move),
	awful.button({modkey}, 3, awful.mouse.client.resize)
)


-- ===================================================================
-- Desktop Key bindings
-- ===================================================================


keys.globalkeys = gears.table.join(
	-- =========================================
	-- SPAWN APPLICATION KEY BINDINGS
	-- =========================================

	
	-- Spawn System-Monitor
	awful.key({altkey, "Control"}, "q",
		function()
			awful.spawn("gnome-system-monitor")
		end,
		{description = "System-Monitor", group = "launcher"}
	),
	
	-- Spawn Schildi-chat
	awful.key({altkey, "Control"}, "s",
		function()
			awful.spawn("schildichat-desktop")
		end,
		{description = "Schildichat", group = "launcher"}
	),
	
	-- Spawn Discord
	awful.key({altkey, "Control"}, "d",
		function()
			awful.spawn("discord")
		end,
		{description = "discord", group = "launcher"}
	),
	
	-- Spawn Telegram
	awful.key({altkey, "Control"}, "t",
		function()
			awful.spawn("telegram-desktop")
		end,
		{description = "telegram", group = "launcher"}
	),
	
	-- Spawn terminal
	awful.key({modkey}, "Return",
		function()
			awful.spawn(apps.terminal)
		end,
		{description = "open a terminal", group = "launcher"}
	),
	
	-- Spawn terminal
	awful.key({altkey, "Control"}, "Return",
		function()
			awful.spawn(apps.terminal)
		end,
		{description = "open a terminal", group = "launcher"}
	),
	
	-- launch rofi
	awful.key({modkey}, "space",
		function()
			awful.spawn(apps.launcher)
		end,
		{description = "application launcher", group = "launcher"}
	),
	
	-- launch file manager
	awful.key({altkey, "Control"}, "e",
		function()
			awful.spawn(apps.filebrowser)
		end,
		{description = "file manager", group = "launcher"}
	),
	
	-- launch keepassxc
	awful.key({altkey, "Control"}, "k",
		function()
			awful.spawn("keepassxc")
		end,
		{description = "keepassxc", group = "launcher"}
	),
	
	-- launch firefox
	awful.key({altkey, "Control"}, "f",
		function()
			awful.spawn("firefox")
		end,
		{description = "firefox", group = "launcher"}
	),


	-- =========================================
	-- FUNCTION KEYS
	-- =========================================

	-- Brightness
	awful.key({}, "XF86MonBrightnessUp",
		function()
			awful.spawn("brightnessctl set +100", false)
		end,
		{description = "+10%", group = "hotkeys"}
	),
	awful.key({}, "XF86MonBrightnessDown",
		function()
			awful.spawn("brightnessctl set 100-", false)
		end,
		{description = "-10%", group = "hotkeys"}
	),

	-- ALSA volume control
	awful.key({}, "XF86AudioRaiseVolume",
		function()
			awful.spawn("pactl set-sink-volume 0 +5%", false)
			awesome.emit_signal("volume_change")
		end,
		{description = "volume up", group = "hotkeys"}
	),
	awful.key({}, "XF86AudioLowerVolume",
		function()
			awful.spawn("pactl set-sink-volume 0 -5%", false)
			awesome.emit_signal("volume_change")
		end,
		{description = "volume down", group = "hotkeys"}
	),
	awful.key({}, "XF86AudioMute",
		function()
			awful.spawn("amixer -D pulse set Master 1+ toggle", false)
			awesome.emit_signal("volume_change")
		end,
		{description = "toggle mute", group = "hotkeys"}
	),
	awful.key({}, "XF86AudioNext",
		function()
			awful.spawn("playerctl next", false)
		end,
		{description = "next music", group = "hotkeys"}
	),
	awful.key({}, "XF86AudioPrev",
		function()
			awful.spawn("playerctl previous", false)
		end,
		{description = "previous music", group = "hotkeys"}
	),
	awful.key({}, "XF86AudioPlay",
		function()
			awful.spawn("playerctl play-pause", false)
		end,
		{description = "play/pause music", group = "hotkeys"}
	),

	-- Screenshot on prtscn using scrot
	awful.key({}, "Print",
		function()
			awful.util.spawn(apps.screenshot, false)
		end
	),

	-- =========================================
	-- RELOAD / QUIT AWESOME
	-- =========================================

	-- Reload Awesome
	awful.key({modkey, "Shift"}, "r",
		awesome.restart,
		{description = "reload awesome", group = "awesome"}
	),

	-- Quit Awesome
	awful.key({modkey}, "Escape",
		function()
			-- emit signal to show the exit screen
			awesome.emit_signal("show_exit_screen")
		end,
		{description = "toggle exit screen", group = "hotkeys"}
	),

	awful.key({}, "XF86PowerOff",
		function()
			-- emit signal to show the exit screen
			awesome.emit_signal("show_exit_screen")
		end,
		{description = "toggle exit screen", group = "hotkeys"}
	),

	-- =========================================
	-- CLIENT FOCUSING
	-- =========================================

	-- Focus client by index (cycle through clients)
	awful.key({modkey}, "d",
		function()
			awful.client.focus.byidx(1)
		end,
		{description = "focus next by index", group = "client"}
	),
	awful.key({modkey}, "a",
		function()
			awful.client.focus.byidx(-1)
		end,
		{description = "focus previous by index", group = "client"}
	),

	awful.key({modkey}, "Right",
		function()
			awful.client.focus.byidx(1)
		end,
		{description = "focus next by index", group = "client"}
	),
	awful.key({modkey}, "Left",
		function()
			awful.client.focus.byidx(-1)
		end,
		{description = "focus previous by index", group = "client"}
	),
	
	-- =========================================
	-- SCREEN FOCUSING
	-- =========================================

	-- Focus screen by index (cycle through screens)
	awful.key({modkey}, "e",
		function()
			awful.screen.focus_relative(1)
		end
	),

	-- =========================================
	-- CLIENT RESIZING
	-- =========================================

	awful.key({modkey, "Control"}, "Down",
		function(c)
			resize_client(client.focus, "down")
		end
	),
	awful.key({modkey, "Control"}, "Up",
		function(c)
			resize_client(client.focus, "up")
		end
	),
	awful.key({modkey, "Control"}, "Left",
		function(c)
			resize_client(client.focus, "left")
		end
	),
	awful.key({modkey, "Control"}, "Right",
		function(c)
			resize_client(client.focus, "right")
		end
	),
	awful.key({modkey, "Control"}, "s",
		function(c)
			resize_client(client.focus, "down")
		end
	),
	awful.key({ modkey, "Control" }, "w",
		function(c)
			resize_client(client.focus, "up")
		end
	),
	awful.key({modkey, "Control"}, "a",
		function(c)
			resize_client(client.focus, "left")
		end
	),
	awful.key({modkey, "Control"}, "d",
		function(c)
			resize_client(client.focus, "right")
		end
	),

	-- =========================================
	-- LAYOUT SELECTION
	-- =========================================

	-- select next layout
	awful.key({modkey}, "Tab",
		function()
			awful.layout.inc(1)
		end,
		{description = "select next", group = "layout"}
	),
	
	-- select previous layout
	awful.key({modkey, "Shift"}, "Tab",
		function()
			awful.layout.inc(-1)
		end,
		{description = "select previous", group = "layout"}
	),
	
	-- =========================================
	-- LAYOUT SELECTION
	-- =========================================
	
	-- next & previous tag
	awful.key({modkey}, "Down",
		function()
			awful.tag.viewnext()
		end,
		{description = "view next tag", group = "tag"}
	),
	
	awful.key({modkey}, "s",
		function()
			awful.tag.viewnext()
		end,
		{description = "view next tag", group = "tag"}
	),
	
	awful.key({modkey}, "Up",
		function()
			awful.tag.viewprev()
		end,
		{description = "view prev tag", group = "tag"}
	),
	
	awful.key({modkey}, "w",
		function()
			awful.tag.viewprev()
		end,
		{description = "view prev tag", group = "tag"}
	),
	
	awful.key({modkey, "Shift"}, "Down",
		function()
			-- get current tag
			local t = client.focus and client.focus.first_tag or nil
			if t == nil then
				return
			end
			-- get next tag
			local tag = client.focus.screen.tags[(t.name % 9) +1 ]
			awful.client.movetotag(tag)
			awful.tag.viewnext()
		end,
		{description = "move client to next tag", group = "client"}
	),
	
	awful.key({modkey, "Shift"}, "s",
		function()
			-- get current tag
			local t = client.focus and client.focus.first_tag or nil
			if t == nil then
				return
			end
			-- get next tag
			local tag = client.focus.screen.tags[(t.name % 9) +1 ]
			awful.client.movetotag(tag)
			awful.tag.viewnext()
		end,
		{description = "move client to next tag", group = "client"}
	),
	
	awful.key({modkey, "Shift"}, "Up",
		function()
			-- get current tag
			local t = client.focus and client.focus.first_tag or nil
			if t == nil then
				return
			end
			-- get next tag
			local tag = client.focus.screen.tags[(t.name -2) %9 +1 ]
			awful.client.movetotag(tag)
			awful.tag.viewprev()
		end,
		{description = "move client to prev tag", group = "client"}
	),
	
	awful.key({modkey, "Shift"}, "w",
		function()
			-- get current tag
			local t = client.focus and client.focus.first_tag or nil
			if t == nil then
				return
			end
			-- get next tag
			local tag = client.focus.screen.tags[(t.name -2) %9 +1 ]
			awful.client.movetotag(tag)
			awful.tag.viewprev()
		end,
		{description = "move client to prev tag", group = "client"}
	)
)


-- ===================================================================
-- Client Key bindings
-- ===================================================================


keys.clientkeys = gears.table.join(
	awful.key({modkey, "Shift"}, "Left",
		function(c)
			awful.client.swap.byidx (-1)
		end
	),
	awful.key({modkey, "Shift"}, "Right",
		function(c)
			awful.client.swap.byidx (1)
		end
	),
	awful.key({modkey, "Shift"}, "a",
		function(c)
			awful.client.swap.byidx (-1)
		end
	),
	awful.key({modkey, "Shift"}, "d",
		function(c)
			awful.client.swap.byidx (1)
		end
	),

	-- toggle fullscreen
	awful.key({modkey}, "f",
		function(c)
			c.fullscreen = not c.fullscreen
		end,
		{description = "toggle fullscreen", group = "client"}
	),

	-- close client
	awful.key({modkey}, "q",
		function(c)
			c:kill()
		end,
		{description = "close", group = "client"}
	)
)

-- Bind all key numbers to tags
for i = 1, 9 do
	keys.globalkeys = gears.table.join(keys.globalkeys,
		-- Switch to tag
		awful.key({modkey}, "#" .. i + 9,
			function()
				local screen = awful.screen.focused()
				local tag = screen.tags[i]
				if tag then
					tag:view_only()
				end
			end,
			{description = "view tag #"..i, group = "tag"}
		),
		-- Move client to tag
		awful.key({modkey, "Shift"}, "#" .. i + 9,
			function()
				if client.focus then
					local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:move_to_tag(tag)
						tag:view_only()
					end
				end
			end,
			{description = "move focused client to tag #"..i, group = "tag"}
		)
	)
end

return keys

