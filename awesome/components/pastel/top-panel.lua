--      ████████╗ ██████╗ ██████╗     ██████╗  █████╗ ███╗   ██╗███████╗██╗
--      ╚══██╔══╝██╔═══██╗██╔══██╗    ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║
--         ██║   ██║   ██║██████╔╝    ██████╔╝███████║██╔██╗ ██║█████╗  ██║
--         ██║   ██║   ██║██╔═══╝     ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║
--         ██║   ╚██████╔╝██║         ██║     ██║  ██║██║ ╚████║███████╗███████╗
--         ╚═╝    ╚═════╝ ╚═╝         ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝

-- ===================================================================
-- Initialization
-- ===================================================================


local awful = require("awful")
local beautiful = require("beautiful")
local wibox = require("wibox")
local gears = require("gears")
local dpi = beautiful.xresources.apply_dpi

-- import buttons
local awesomebuttons = require("awesome-buttons.awesome-buttons")

-- import widgets
local task_list = require("widgets.task-list")

-- define module table
local top_panel = {}


-- ===================================================================
-- Bar Creation
-- ===================================================================


top_panel.create = function(s)
	local panel = awful.wibar({
		screen = s,
		position = "top",
		ontop = true,
		height = beautiful.top_panel_height,
		width = s.geometry.width,
	})

	panel:setup {
		expand = "none",
		layout = wibox.layout.align.horizontal,
		{
		layout = wibox.layout.fixed.horizontal,
		awesomebuttons.with_icon{ 
			icon = '/home/quantenzitrone/.config/awesome/icons/arch.svg', 
			icon_size = beautiful.left_panel_width,
			icon_margin = 0,
			shape = "circle",
			color = '#00000000',
			onclick = function()
				awful.spawn(apps.launcher)
			end
			},
		--space
		wibox.widget {
			widget = wibox.widget.separator,
			color = "#00000000",
			forced_width = 12,
		},
			task_list.create(s),
		},
		wibox.widget {
			widget = wibox.widget.separator,
			color = "#00000000",
			forced_width = 12,
		},
		require("widgets.calendar").create(s)
	}


	-- ===================================================================
	-- Functionality
	-- ===================================================================


	-- hide panel when client is fullscreen
	local function change_panel_visibility(client)
		if client.screen == s then
			panel.ontop = not client.fullscreen
		end
	end

	-- connect panel visibility function to relevant signals
	client.connect_signal("property::fullscreen", change_panel_visibility)
	client.connect_signal("focus", change_panel_visibility)

end

return top_panel
