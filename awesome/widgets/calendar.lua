--       ██████╗ █████╗ ██╗     ███████╗███╗   ██╗██████╗  █████╗ ██████╗
--      ██╔════╝██╔══██╗██║     ██╔════╝████╗  ██║██╔══██╗██╔══██╗██╔══██╗
--      ██║     ███████║██║     █████╗  ██╔██╗ ██║██║  ██║███████║██████╔╝
--      ██║     ██╔══██║██║     ██╔══╝  ██║╚██╗██║██║  ██║██╔══██║██╔══██╗
--      ╚██████╗██║  ██║███████╗███████╗██║ ╚████║██████╔╝██║  ██║██║  ██║
--       ╚═════╝╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═══╝╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝


-- ===================================================================
-- Initialization
-- ===================================================================


local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi

local calendar = {}


-- ===================================================================
-- Create Widget
-- ===================================================================


calendar.create = function(screen)
   -- Clock / Calendar 12h format
   -- Get Time/Date format using `man strftime`
   local clock_widget = wibox.widget.textclock("<span font='" .. beautiful.title_font .."'> %a %F   %T  </span>", 1)

   -- Round
   local cal_shape = function(cr, width, height)
      gears.shape.partially_rounded_rect(cr, width, height, true, true, true, true, 12)
   end
   
   -- Round 2
   local cal_shape2 = function(cr, width, height)
      gears.shape.partially_rounded_rect(cr, width, height, true, true, true, true, 5)
   end

   -- Calendar Widget
   local month_calendar = awful.widget.calendar_popup.month({
      screen = screen,
      start_sunday = false,
      spacing = 10,
      font = beautiful.title_font, 
      position = "tr",
      long_weekdays = True,
      margin = 0, -- 10
      style_month = {border_width = 0, padding = 12, shape = cal_shape, padding = 30, bg_color = "#00000088"},
      style_header = {border_width = 0, bg_color = "#00000000"},
      style_weekday = {border_width = 0, bg_color = "#00000000"},
      style_normal = {border_width = 0, bg_color = "#00000000", align = "center", ignore_marku=false},
      style_focus = {border_width = 0, shape = cal_shape2, bg_color = "#880000dd", align = "center"},
   })

   -- Attach calentar to clock_widget
   month_calendar:attach(clock_widget, "tc" , { on_pressed = false, on_hover = true })

   return clock_widget
end

return calendar
